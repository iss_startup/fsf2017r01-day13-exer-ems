
// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches EmpService service to the EMS module
    angular
        .module("EMS")
        .service("EmpService", EmpService);

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    EmpService.$inject = ['$http'];

    // EmpService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function EmpService($http) {

        // Declares the var service and assigns it the object this (in this case, the EmpService). Any function or
        // variable that you attach to service will be exposed to callers of EmpService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        // EXPOSED DATA MODELS -----------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
        service.insertEmp = insertEmp;
        service.retrieveEmp = retrieveEmp;
        service.retrieveEmpByNo = retrieveEmpByNo;
        service.retrieveEmpDB = retrieveEmpDB;
        service.retrieveEmpDept = retrieveEmpDept;
        service.updateEmpName = updateEmpName ;
        service.deleteEmp = deleteEmp;

        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------
        // deleteDept uses HTTP DELETE to delete department from database; passes information as route parameters.
        // IMPORTANT! Route parameters are not the same as query strings!
        function deleteEmp(emp_no) {
            return $http({
                method: 'DELETE'
                , url: 'api/employees/' +  emp_no
            });

        }


        // insertEmp uses HTTP POST to send employee information to the server's /employees route
        // Parameters: employee information; Returns: Promise object
        function insertEmp(employee) {
            // This line returns the $http to the calling function
            // This configuration specifies that $http must send the employee data received from the calling function
            // to the /employees route using the HTTP POST method. $http returns a promise object. In this instance
            // the promise object is returned to the calling function
            return $http({
                method: 'POST'
                , url: 'api/employees'
                , data: {emp: employee}
            });
        }

        // retrieveEmp retrieves employee information from the server via HTTP GET.
        // Parameters: none. Returns: Promise object
        function retrieveEmp(){
            return $http({
                method: 'GET'
                , url: 'api/static/employees'
            });
        }

        function retrieveEmpByNo(emp_no) {
            return $http({
                method: 'GET'
                , url: 'api/employees/' + emp_no
            });
        }


        // retrieveEmpDB retrieves employee information from the server via HTTP GET. Passes information via the query
        // string (params) Parameters: searchString. Returns: Promise object
        function retrieveEmpDB(searchString){
            return $http({
                method: 'GET'
                , url: 'api/employees'
                , params: {
                    'searchString': searchString
                }
            });
        }

        // retrieveEmpDept retrieves employee and department information from the server via HTTP GET.
        // Parameters: searchString. Returns: Promise object
        function retrieveEmpDept(searchString){
            return $http({
                method: 'GET'
                , url: 'api/employees/departments'
                , params: {
                    'searchString': searchString
                }
            });
        }

        function updateEmpName( emp_no, first_name) {
            return $http({
                method: 'PUT'
                , url: 'api/employees/' + emp_no
                , data: {
                    first_name: first_name
                }
            });
        }


    }
})();