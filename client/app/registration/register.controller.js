//TODO: Add ability to choose department when registering an employee
//TODO: Saving of employee in employees table and of department in departments table should be
//TODO: one  transaction (i.e., atomic)-->
//TODO: 2. Allow users to choose a department from a list of employees. Note: Function to call is in DeptService

(function() {
    angular
        .module("EMS")
        .controller("RegCtrl", RegCtrl);

    // TODO: 2.1 Inject DeptService so we can access appropriate function
    RegCtrl.$inject = [ '$window', 'EmpService' ];

    function RegCtrl( $window, EmpService) {

         var vm = this;
        var today = new Date();
        var birthday = new Date();
        birthday.setFullYear(birthday.getFullYear() - 18);

          vm.employee = {
            empNo: "",
            firstname: "",
            lastname: "",
            gender: "M",
            birthday: birthday,
            hiredate: today,
            phonenumber: "",

        };

        vm.status = {
            message: "",
            code: ""
        };


        vm.register = register;

        // TODO: 2.2 Create function that would populate the department selection box with data. Logic should be placed
        // TODO: 2.2 in DeptService, but handling of success/error should be done in this controller

        function register() {
            alert("The registration information you sent are \n" + JSON.stringify(vm.employee));

            console.log("The registration information you sent were:");
            console.log("Employee Number: " + vm.employee.empNo);
            console.log("Employee First Name: " + vm.employee.firstname);
            console.log("Employee Last Name: " + vm.employee.lastname);
            console.log("Employee Gender: " + vm.employee.gender);
            console.log("Employee Birthday: " + vm.employee.birthday);
            console.log("Employee Hire Date: " + vm.employee.hiredate);
            console.log("Employee Phone Number: " + vm.employee.phonenumber);

         EmpService
                .insertEmp(vm.employee)
                .then(function (result) {
                    console.log("result " + JSON.stringify(result));
                    $window.location.assign('/app/registration/thanks.html');
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent.errno;
                });

        }
    }
})();