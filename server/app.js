//TODO: Add ability to choose department when registering an employee.-->
//TODO: Saving of employee in employees table and of department in departments table should be-->

// TODO: 4. Add route that would handle retrieval of departments to client
// TODO: 6. Update POST api/employees route to include insertion of new employee and its department into the
// TODO: 6. dept_manager table


var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");

// Loads sequelize ORM
var Sequelize = require("sequelize");

const NODE_PORT = process.env.NODE_PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname + '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'my_password';


var app = express();

app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());


var sequelize = new Sequelize(
    'employees',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost',         // default port    : 3306
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

var Employee = require('./models/employees')(sequelize, Sequelize);

var Department = require('./models/departments')(sequelize, Sequelize);
var DeptEmp = require('./models/deptemp')(sequelize, Sequelize);

Employee.hasMany(DeptEmp, {foreignKey: 'emp_no'})
//DeptEmp.hasOne(Department, {foreignKey: 'dept_no'});
DeptEmp.belongsTo(Department, {foreignKey: 'dept_no'});


app.post("/api/employees", function (req, res, next) {
    console.log('\nInformation submitted to server:')
    console.log(req.body);

    // TODO: 6.1 Comment out current create statement. Do not delete so that you could refer to it in the future
    Employee
        .create({
            emp_no: req.body.emp.empNo,
            birth_date: new Date(req.body.emp.birthday),
            first_name: req.body.emp.firstname,
            last_name: req.body.emp.lastname,
            gender: req.body.emp.gender,
            hire_date: new Date(req.body.emp.hiredate)
        })
        .then(function (employee) {
            res
                .status(200)
                .json(employee);
        })
        .catch(function (err) {
            console.log(err);
            res
                .status(501)
                .json(err);
        });

    // TODO: 6.2 Create a transaction (atomic operation) where employee record is created only when an equivalent
    // TODO: 6.2 department record is created

});

app.get("/api/employees", function (req, res) {
    Employee
        .findAll({
            where: {

                $or: [
                    {first_name: {$like: "%" + req.query.searchString + "%"}},
                    {last_name: {$like: "%" + req.query.searchString + "%"}},
                    {emp_no: {$like: "%" + req.query.searchString + "%"}}
                ]
            }
            // We add a limit since employees table is big
            , limit: 100
        })
        .then(function (employees) {
            res
                .status(200)
                .json(employees);
        })
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});


app.get("/api/employees/departments", function (req, res) {
    Employee
        .findAll({
            where: {
                $or: [
                    {first_name: {$like: "%" + req.query.searchString + "%"}},
                    {last_name: {$like: "%" + req.query.searchString + "%"}},
                    {emp_no: {$like: "%" + req.query.searchString + "%"}}
                ]
            }
            , limit: 100
            , include: [{
                model: DeptEmp
                , order: [["to_date", "DESC"]]
                , limit: 1
                , include: [Department]
            }]
        })
        .then(function (employees) {
            res
                .status(200)
                .json(employees);
        })
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});


// -- Searches for specific employees by emp_no

app.get("/api/employees/:emp_no", function (req, res) {
    console.log
    var where = {};
    if (req.params.emp_no) {
        where.emp_no = req.params.emp_no
    }

    console.log("where " + where);
    Employee
        .findOne({
            where: where
            , include: [{
                model: DeptEmp
                , order: [["to_date", "DESC"]]
                , limit: 1
                // We include the Employee model to get the manager's name
                , include: [Department]
            }]
        })

        .then(function (employees) {
            console.log("-- GET /api/employees/:emp_no findOne then() result \n " + JSON.stringify(employees));
            res.json(employees);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            console.log("-- GET /api/employees/:emp_no findOne catch() \n ");

            res
                .status(500)
                .json({error: true});
        });
});


app.put('/api/employees/:emp_no', function (req, res) {
    var where = {};
    where.emp_no = req.params.emp_no;

    // Updates employee detail
    console.log("body " + JSON.stringify(req.body));
    Employee
        .update(
            {first_name: req.body.first_name}
            , {where: where}                            // search condition / criteria
        )
        .then(function (employee) {
            res
                .status(200)
                .json(employee);
        })
        .catch(function (err) {
            console.log(err);
            res
                .status(500)
                .json(err);
        });
});



app.delete("/api/employees/:emp_no", function (req, res) {
    var where = {};
    where.emp_no = req.params.emp_no;

    Employee
        .destroy({
            where: where
        })
        .then(function (result) {
            if (result == "1")
                res.json({success: true});
            else
                res.json({success: false});
        })
        .catch(function (err) {
            console.log("-- DELETE /api/employees/:emp_no catch(): \n" + JSON.stringify(err));
        });
});



app.get("/api/static/employees", function (req, res) {
    var employees = [
        {
            empNo: 1001,
            empFirstName: 'Emily',
            empLastName: 'Smith',
            empPhoneNumber: '6516 2093'

        }
        , {
            empNo: 1002,
            empFirstName: 'Varsha',
            empLastName: 'Jansen',
            empPhoneNumber: '6516 2093'
        }
        ,
        {
            empNo: 1003,
            empFirstName: 'Julie',
            empLastName: 'Black',
            empPhoneNumber: '6516 2093'
        }
        , {
            empNo: 1004,
            empFirstName: 'Fara',
            empLastName: 'Johnson',
            empPhoneNumber: '6516 2093'
        }
        ,
        {
            empNo: 1005,
            empFirstName: 'Justin',
            empLastName: 'Zhang',
            empPhoneNumber: '6516 2093'
        }
        , {
            empNo: 1006,
            empFirstName: 'Kenneth',
            empLastName: 'Black',
            empPhoneNumber: '6516 2093'
        }

    ];

    res
        .status(200)
        .json(employees);
});

// TODO: 4. Add route that would handle retrieval of departments that are not managers and would return name and no
// TODO: to client

});

app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});